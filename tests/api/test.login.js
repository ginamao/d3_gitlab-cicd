var assert = require('assert');
var request = require('supertest');
var server = require("../../index");
var agent = request(server);

describe('Test Login API', function () {
    var path = "/login";

    var testValidUser = {
        username: 'validuser',
        password: 'strongpassword'
    }

    var testInvalidUser = {
        username: 'invaliduser',
        password: 'wrongpassword'
    }

    it('[驗證身份] 合法使用者，須回傳 200', function(done) {
        agent
            .post(path)
            .send(testValidUser)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                done();
            })
    });

    it('[驗證身份] 非法使用者，須回傳 401', function(done) {
        agent
            .post(path)
            .send(testInvalidUser)
            .expect(401)
            .end(function (err, res) {
                if (err) return done(err);
                done();
            })
    });
})