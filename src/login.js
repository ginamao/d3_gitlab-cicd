/**
 *
 * @param {string} password input password
 * @returns {boolen} true: password is correct; false: password is wrong
 */
function checkPsw(password) {
    return (password == "strongpassword")? true : false;
}

/**
 *
 * @param {string} username Who visits our website
 * @returns {string} Welcome message
 */
function getHelloMsg(username) {
    return `Hello, ${username}! Welcome to our website.`;
}

exports.checkPsw = checkPsw;
exports.getHelloMsg = getHelloMsg;